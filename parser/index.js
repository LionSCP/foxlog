const { formatDate } = require('./helpers')

/**
 * Take a line string and parse to convert in object manipulable
 * @param {String} string the read line from stream
 * @returns {Object}
 */
const parseLine = (line) => {
  const splittedLine = line.toString().split(' ')

  return {
    host: splittedLine[0],
    logname: splittedLine[2],
    date: formatDate(splittedLine[3]),
    method: splittedLine[5].replace('"', ''),
    section: splittedLine[6].split('/')[1],
    protocol: splittedLine[7].split('/')[0],
    status: Number(splittedLine[8]),
    bytes: Number(splittedLine[9])
  }
}

module.exports = {
  parseLine
}
