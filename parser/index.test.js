const { parseLine } = require('./index');
const { getDate, formatDate } = require('./helpers');

describe('Parser module', () => {
  test('take a line from stream and parse it to an object', () => {
    const line = '248.255.77.144 - Matteo81 [16/Feb/2020:21:28:15 +0100] "HEAD /section1/sub2 HTTP/1.0" 258 236';
    expect(parseLine(line)).toEqual({
      host: '248.255.77.144',
      logname: 'Matteo81',
      date: 1581884895000,
      method: 'HEAD',
      section: 'section1',
      protocol: 'HTTP',
      status: 258,
      bytes: 236,
    });
  });

  test('extract the date from read line', () => {
    const parsedLine = '[16/Feb/2020:21:28:15';
    expect(getDate(parsedLine)).toEqual([
      '16/Feb/2020',
      '21:28:15',
    ]);
  });

  test('format date to get the unix time', () => {
    const parsedLine = '[16/Feb/2020:21:28:15';
    expect(formatDate(parsedLine)).toBe(1581884895000);
  });
});
