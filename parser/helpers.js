const moment = require('moment')

/**
 * Take a line string and convert to a readable date
 * @param {String} string the read line from stream
 * @returns {Array<String>}
 */
const getDate = (string) => {
  const trimDate = string.replace('[', '')
  return [trimDate.slice(0, trimDate.indexOf(':')), trimDate.slice(trimDate.indexOf(':') + 1, trimDate.length)]
}

/**
 * Take a line string and convert to ISO date and unix time
 * @param {String} string the read line from stream
 * @returns {Number}
 */
const formatDate = (string) => {
  const cleanedDate = getDate(string)
  const formattedDate = `${moment(cleanedDate[0], 'DD/MMM/YYYY').format('YYYY-MM-DD')}T${cleanedDate[1]}`
  return new Date(formattedDate).getTime()
}

module.exports = {
  formatDate,
  getDate
}
