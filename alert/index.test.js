const { handleOverload } = require('./index');

describe('Handling overload', () => {
  test('Average is higher than threshold and test if push an alert to array', () => {
    const journal = [];
    const count = 150;
    const threshold = 10;
    const alertInterval = 10000;
  
    const newJournal = handleOverload(journal, count, threshold, alertInterval);
  
    expect(newJournal.length).toBe(1);
  });

  test('Average is lower than threshold and test if none alert has pushed to array', () => {
    const journal = [];
    const count = 80;
    const threshold = 10;
    const alertInterval = 10000;
  
    const newJournal = handleOverload(journal, count, threshold, alertInterval);
  
    expect(newJournal.length).toBe(0);
  });

})
