/**
 * This is the function that monitors if requests overloads the system
 * @param {Array<String>} journal an array of alerts
 * @param {Number} count the current count of requests
 * @param {Number} threshold warning threshold
 * @param {Number} alertInterval monitoring interval
 * @returns {Array<String>}
 */
const handleOverload = (journal, count, threshold, alertInterval) => {
  const average = count * 1000 / alertInterval

  if (average > threshold) {
    journal.push(`High traffic generated an alert - hits = ${average.toFixed(2)}, triggered at ${new Date().toLocaleString()}`)
  }

  return journal
}

module.exports = {
  handleOverload
}
