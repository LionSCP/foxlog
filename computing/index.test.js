const { getCleanedData, computingRawData, getMethodsRates, getDataOfBytes, getDataOfCounts, getDataOfErrors, sortData } = require('./computing');
const { manageComputation, manageStream, manageLogBehavior } = require('./index');

const mockLogs = require('../__mocks__/logs');
const rawData = require('../__mocks__/rawData');
const sortedData = require('../__mocks__/sortedData');
const cleanedData = require('../__mocks__/cleanedData');

describe('Computing module', () => {
  test('first step of manager, that decides what behavior read line will have ', () => {
    const state = {
      logs: {},
      storeOfReceivedLogs: [],
      tempNumberOfLogs: 0,
      alerts: []
    };

    const line = '63.60.59.143 - Hertha.Kub78 [25/Feb/2020:09:03:31 +0100] "GET /section5/sub5 HTTP/1.0" 544 119'

    expect(manageStream(line, state)).resolves.toEqual({
      logs: {
        section5:
          [{
            host: '63.60.59.143',
            logname: 'Hertha.Kub78',
            date: 1582617811000,
            method: 'GET',
            section: 'section5',
            protocol: 'HTTP',
            status: 544,
            bytes: 119
          }]
      },
      storeOfReceivedLogs:
        ['63.60.59.143 - Hertha.Kub78 [25/Feb/2020:09:03:31 +0100] "GET /section5/sub5 HTTP/1.0" 544 119'],
      tempNumberOfLogs: 0,
      alerts: []
    })
  });

  test('second step of manager, this affetcs a line in its section', () => {
    const line = '53.254.241.217 - Dillan36 [25/Feb/2020:09:03:31 +0100] "POST /section3/sub5 HTTPS/1.0" 326 135';

    const logs = {
      section5:
        [{
          host: '63.60.59.143',
          logname: 'Hertha.Kub78',
          date: 1582617811000,
          method: 'GET',
          section: 'section5',
          protocol: 'HTTP',
          status: 544,
          bytes: 119
        }]
    };

    expect(manageLogBehavior(logs, line)).toEqual({
      section5:
        [{
          host: '63.60.59.143',
          logname: 'Hertha.Kub78',
          date: 1582617811000,
          method: 'GET',
          section: 'section5',
          protocol: 'HTTP',
          status: 544,
          bytes: 119
        }],
      section3:
        [{
          host: '53.254.241.217',
          logname: 'Dillan36',
          date: 1582617811000,
          method: 'POST',
          section: 'section3',
          protocol: 'HTTPS',
          status: 326,
          bytes: 135
        }]
    });
  });

  test('last step to get the cleaned data from logs state', () => {

    expect(manageComputation(mockLogs)).toEqual(cleanedData);
  });

  test('first step of computation, that convert logs into manipulable data', () => {
    expect(computingRawData(mockLogs)).toEqual(sortedData);
  });

  test('sort data from raw data to get arrays sorted by counts, errors and bytes', () => {
    expect(sortData(rawData)).toEqual(sortedData);
  });

  test('test if function clean data from sorted data', () => {
    expect(getCleanedData(sortedData)).toEqual(cleanedData);
  });

  test('test if function get clean data of counts from sorted data of counts', () => {
    expect(getDataOfCounts(sortedData.sortedByCounts)).toEqual(cleanedData.dataOfCounts);
  });

  test('test if function get clean data of errors from sorted data of errors', () => {
    expect(getDataOfErrors(sortedData.sortedByErrors)).toEqual(cleanedData.dataOfErrors);
  });

  test('test if function get clean data of bytes from sorted data of bytes', () => {
    expect(getDataOfBytes(sortedData.sortedByBytes)).toEqual(cleanedData.dataOfBytes);
  });

  test('get the rates from http methods raw data', () => {
    const httpMethods = { POST: 45, GET: 90, HEAD: 34, PUT: 6, PATCH: 12, DELETE: 44 };
    const total = 231;
    expect(getMethodsRates(httpMethods, total)).toEqual({ 
      POST: 19.48,
      GET: 38.96,
      HEAD: 14.72,
      PUT: 2.6,
      PATCH: 5.19,
      DELETE: 19.05 })
  })
});