const httpMethods = {
  get: 'GET',
  post: 'POST',
  put: 'PUT',
  patch: 'PATCH',
  delete: 'DELETE',
  head: 'HEAD'
}

const httpProtocol = {
  http: 'HTTP',
  https: 'HTTPS'
}

module.exports = {
  httpMethods,
  httpProtocol
}
