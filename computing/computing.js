const { httpMethods, httpProtocol } = require('./enums')
const { getPercentage, sortArrayByQuery } = require('../utils')

/**
 * Take httpMethods object and calculates rates in relation to section total logs
 * @param {Object} httpMethods object with detail of count by httpMethods
 * @param {Number} total count of section total logs
 * @returns {Object}
 */
const getMethodsRates = (httpMethods, total) => {
  Object.keys(httpMethods).forEach((key) => {
    httpMethods[key] = getPercentage(httpMethods[key], total)
  })

  return httpMethods
}

/**
 * Take logs object and calculates a lot of kpi's
 * @param {Object} logs an object with logs
 * @returns {Object}
 */
const computingRawData = (logs) => {
  const rawData = []
  const sections = Object.keys(logs)

  for (const section of sections) {
    const countLogs = logs[section].length
    let errors = 0
    let bytes = 0
    const requestCountByMethods = {
      [httpMethods.post]: 0,
      [httpMethods.get]: 0,
      [httpMethods.head]: 0,
      [httpMethods.put]: 0,
      [httpMethods.patch]: 0,
      [httpMethods.delete]: 0
    }
    const requestCountByProtocol = {
      [httpProtocol.http]: 0,
      [httpProtocol.https]: 0
    }

    logs[section].forEach((element) => {
      requestCountByMethods[element.method] += 1
      requestCountByProtocol[element.protocol] += 1
      if (element.status >= 400 || element.status >= 500) errors++
      bytes += element.bytes
    })

    const result = {
      section,
      countLogs,
      errors,
      errorPercentage: getPercentage(errors, countLogs),
      requestCountByProtocol,
      protocolPercentage: {
        http: getPercentage(requestCountByProtocol[httpProtocol.http], countLogs),
        https: getPercentage(requestCountByProtocol[httpProtocol.https], countLogs)
      },
      requestCountByMethods,
      methodsPercentage: getMethodsRates(requestCountByMethods, countLogs),
      bytes,
      averageWeightOfRequest: Math.round(bytes / countLogs) * 100 / 100
    }

    rawData.push(result)
  }
  return sortData(rawData)
}

/**
 * Take an array of collected raw data and sort them by type with queries
 * @param {Array<Object>} rawData an array with raw data
 * @returns {Object}
 */
const sortData = (rawData) => {
  const sortedByCounts = sortArrayByQuery(rawData, (a, b) => a.countLogs < b.countLogs)
  const sortedByErrors = sortArrayByQuery(rawData, (a, b) => a.errors < b.errors)
  const sortedByBytes = sortArrayByQuery(rawData, (a, b) => a.averageWeightOfRequest < b.averageWeightOfRequest)

  return {
    sortedByCounts,
    sortedByErrors,
    sortedByBytes
  }
}

/**
 * Clean the data, and preserve only necessary items
 * @param {Object} Object with sorted data in arrays
 * @returns {Object}
 */
const getCleanedData = ({ sortedByCounts, sortedByErrors, sortedByBytes }) => ({
  dataOfCounts: getDataOfCounts(sortedByCounts),
  dataOfErrors: getDataOfErrors(sortedByErrors),
  dataOfBytes: getDataOfBytes(sortedByBytes)
})

/**
 * Clean array to preserve necessary item and manage how much items returns
 * @param {Array<Object>} sortedByCounts array with sorted data by counts
 * @returns {Array<Object>}
 */
const getDataOfCounts = (sortedByCounts) => {
  const dataOfCounts = []

  if (sortedByCounts.length >= 2) {
    for (let i = 0; i <= 2; i++) {
      dataOfCounts.push({
        section: sortedByCounts[i].section,
        count: sortedByCounts[i].countLogs,
        protocol: sortedByCounts[i].protocolPercentage
      })
    }
  } else {
    dataOfCounts.push({
      section: sortedByCounts[0].section,
      count: sortedByCounts[0].countLogs,
      protocol: sortedByCounts[0].protocolPercentage
    })
  }

  return dataOfCounts
}

/**
 * Clean array to preserve necessary item and manage how much items returns
 * @param {Array<Object>} sortedByCounts array with sorted data by counts
 * @returns {Array<Object>}
 */
const getDataOfErrors = (sortedByErrors) => {
  const dataOfErrors = []

  if (sortedByErrors.length >= 2) {
    for (let i = 0; i <= 2; i++) {
      dataOfErrors.push({
        section: sortedByErrors[i].section,
        count: sortedByErrors[i].errors,
        percentage: sortedByErrors[0].errors / sortedByErrors[0].countLogs * 100
      })
    }
  } else {
    dataOfErrors.push({
      section: sortedByErrors[0].section,
      count: sortedByErrors[0].errors,
      percentage: sortedByErrors[0].errors / sortedByErrors[0].countLogs * 100
    })
  }

  return dataOfErrors
}

/**
 * Clean array to preserve necessary item and manage how much items returns
 * @param {Array<Object>} sortedByCounts array with sorted data by counts
 * @returns {Array<Object>}
 */
const getDataOfBytes = (sortedByBytes) => {
  const dataOfBytes = []

  if (sortedByBytes.length >= 2) {
    for (let i = 0; i <= 2; i++) {
      dataOfBytes.push({
        section: sortedByBytes[i].section,
        count: sortedByBytes[i].averageWeightOfRequest
      })
    }
  } else {
    dataOfBytes.push({
      section: sortedByBytes[0].section,
      count: sortedByBytes[0].averageWeightOfRequest
    })
  }

  return dataOfBytes
}

module.exports = {
  computingRawData,
  getCleanedData,
  getMethodsRates,
  getDataOfBytes,
  getDataOfCounts,
  getDataOfErrors,
  sortData
}
