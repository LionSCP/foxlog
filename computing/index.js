const { computingRawData, getCleanedData } = require('./computing')
const { displayResults } = require('../display')
const { parseLine } = require('../parser')

/**
 * Function that monitors changes of file and init computation
 * @param {Object} logs object with all logs
 * @returns {Object}
 */
const manageComputation = (logs) => {
  const rawData = computingRawData(logs)
  return getCleanedData(rawData)
}

/**
 * Manage the stream behavior, when catch a line and procces it
 * @param {String} line a string with the read line
 * @param {Object} state the current state of system
 * @returns {Promise}
 */
const manageStream = (line, state) => {
  return new Promise((resolve, reject) => {
    if (state.storeOfReceivedLogs.every(e => e !== line)) {
      state.storeOfReceivedLogs.push(line)
      const newLogs = manageLogBehavior(state.logs, line)
      state.logs = newLogs
      resolve(state)
    }
  })
}

/**
 * Manage log behavior, to parse it and push on logs store that allows computation
 * @param {Object} logs the current state of logs
 * @param {String} line the current read line
 * @returns {Object}
 */
const manageLogBehavior = (logs, line) => {
  const parsedLine = parseLine(line)
  if (!logs[parsedLine.section]) {
    logs[parsedLine.section] = [parsedLine]
  } else {
    logs[parsedLine.section].push(parsedLine)
  }
  return logs
}

/**
 * Manage behavior of the callback when reading line process is over
 * @param {Object} logs the current state of logs
 * @param {Array} alerts the current state of alerts
 * @param {Number} countOfLogs the current total of read logs
 * @returns {Void}
 */
const manageCloseOfWatching = (logs, alerts, countOfLogs) => {
  const result = manageComputation(logs)
  displayResults(result, alerts, countOfLogs)
}

module.exports = {
  manageComputation,
  manageStream,
  manageCloseOfWatching,
  manageLogBehavior
}
