const yargs = require('yargs')
const { start } = require('./config')
const { convertSecondsToMs } = require('./utils')

const args = yargs
  .option('filename', {
    alias: 'f',
    default: '',
    description: 'Path of log file.',
    type: 'string'
  })
  .option('delay', {
    alias: 'd',
    default: 10,
    description: 'Delay between computation (in seconds)',
    type: 'number'
  })
  .option('threshold', {
    alias: 't',
    default: 10,
    description: 'Maximum average number of hits per seconds. If the threshold is exceeded, it raise an alert',
    type: 'number'
  })
  .option('alertInterval', {
    default: 120,
    description: 'Time interval to handle overload of traffic (in seconds)'
  })
  .help()
  .alias('help', 'h')
  .argv

start({
  ...args,
  delay: convertSecondsToMs(args.delay),
  alertInterval: convertSecondsToMs(args.alertInterval)
})
