# Foxlog

Application that monitors traffic written in a file

## Installation

To start the program, you can run :

```sh
npm install
npm run start -- --filename [path]
```
+ `--filename, -f` : [required] [string] set the the path of file you want to watch

Use the `--help` option to get all information about options

### Options

You can set 3 others options in this program :

+ `--delay, -d` : [number] set the time between each calculation and display updates (in seconds)
+ `--threshold, -t` : [number] set the average of hits per second that trigger an alert
+ `--alertInterval` : [number] set the interval where the program checks overload (in seconds)

Following the default options 

```json
{
  "delay": "10",
  "threshold": "10",
  "alertInterval": "120"
}
```

## Feedback

This technical test was very interesting for me because it does not concern a web project.  
My biggest problem was when I started, because I didn’t know all the tools to monitor a file.  
I had to test several methods before I stopped at this one.  
My idea was to create the process from start to finish, for a single line in order to master the process and then add the calculation.  
I developed the calculations with a mock of logs that I extracted, and I have from the start defined 3 KPI’s: global counting, errors and requests weights. In this way, I was able to orient my calculations. Of course, many other Kpis can be calculated (global ratios for example).  
I have tried to divide the process into a maximum of modules, each of which corresponds to a specific task. This allows you to split the process and iterate on a module independently.  

## Improvements

The program currently reads all lines of the file each time and this can affect performance if the file is too large.  
Although I sort to avoid repeating tasks on past logs, the calculation is done each time on the new log state.  
An improvement would be to do the calculations only on the difference, and to aggregate them with existing calculations.  

## Conclusions

I really enjoyed doing this technical test because it put me in trouble throughout the development, with a peak at the beginning.  

I hope to meet your demands !  