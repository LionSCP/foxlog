const fs = require('fs')

/**
 * This function allows to create a mock of object logs - only used on dev
 * @param path the path of new file
 * @param logs object of logs
 */
const writeMockWithLogs = (path, logs) => {
  fs.writeFile(path, JSON.stringify(logs), (err) => {
    if (err) console.error(err)
    console.log('logs writed')
  })
}

/**
 * This function allows to create a mock of object alerts - only used on dev
 * @param path the path of new file
 * @param alerts array of alerts
 */
const writeMockWithAlerts = (path, alerts) => {
  const mock = {
    alerts
  }
  fs.writeFile(path, JSON.stringify(mock), (err) => {
    if (err) console.error(err)
    console.log('alerts writed')
  })
}

module.exports = {
  writeMockWithLogs,
  writeMockWithAlerts
}
