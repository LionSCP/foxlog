const { print } = require('../utils')

/**
 * Display all messages of dashboard
 * @param {Array} result an array of computation result
 * @param {alerts} alerts an array of alerts
 * @param {Number} count total hits
 * @return {Void}
 */
const displayResults = (result, alerts, count) => {
  console.clear()
  console.log('*    FOXLOG Dashboard')
  console.log('*    by Serge Gonçalves Da Silva')
  console.log(' ')
  console.log(' ')
  console.log(`*        Total logs: ${print(count)}`)
  console.log(' ')
  console.log(`*        Most visited section: ${result ? print(result.dataOfCounts[0].section) : print()}`)
  console.log(`*        number of hit: ${result ? print(result.dataOfCounts[0].count) : print()}`)
  console.log(`*        in https: ${result ? print(result.dataOfCounts[0].protocol.https) : print()} %`)
  console.log(' ')
  if (result && result.dataOfCounts.length > 1) {
    console.log(`*        2nd most visited section: ${print(result.dataOfCounts[1].section)}`)
    console.log(`*        number of hit: ${print(result.dataOfCounts[1].count)}`)
    console.log(' ')
  } else {
    console.log('*        2nd most visited section: unknown')
    console.log('*        number of hit: 0')
    console.log(' ')
  }
  console.log(`*        Section with most errors: ${result ? print(result.dataOfErrors[0].section) : print()}`)
  console.log(`*        number of hits: ${result ? print(result.dataOfErrors[0].count) : print()}`)
  console.log(`*        percentage: ${result ? print(result.dataOfErrors[0].percentage.toFixed(2)) : print()} %`)
  console.log(' ')
  console.log(`*        Section with highest weight average : ${result ? print(result.dataOfBytes[0].section) : print()}`)
  console.log(`*        average: ${result ? print(result.dataOfBytes[0].count) : print()} bytes`)
  console.log(' ')
  console.log(' ')
  console.log('Ctrl+C to exit...')
  console.log(' ')
  console.log(' ')
  console.log(' ')

  if (alerts && alerts.length >= 1) {
    console.log('*        Warnings :')
    console.log(alerts.join('\n'))
  }
}

module.exports = {
  displayResults
}
