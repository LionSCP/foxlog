const fs = require('fs')
const Stream = require('stream')
const readline = require('readline')

/**
 * Start the stream who allows to read line by line
 * @param {String} path path of file that have been monitored
 * @returns {EventEmitter}
 */
const startStream = (path) => {
  const instream = fs.createReadStream(path)
  const outstream = new Stream()
  return readline.createInterface(instream, outstream)
}

module.exports = {
  startStream
}
