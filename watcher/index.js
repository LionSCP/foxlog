const fs = require('fs')

const { handleOverload } = require('../alert')
const { startStream } = require('../stream')
const { manageStream, manageCloseOfWatching } = require('../computing')

/**
 * Monitors changes of file and init computation
 * @param {Object} args args of prompt
 * @returns {Void}
 */
const watch = ({ filename, delay, alertInterval, threshold }) => {
  const state = {
    logs: {},
    storeOfReceivedLogs: [],
    tempNumberOfLogs: 0,
    alerts: []
  }

  setInterval(() => {
    const currentCountOfLogs = state.storeOfReceivedLogs.length - state.tempNumberOfLogs
    state.alerts = handleOverload(state.alerts, currentCountOfLogs, threshold, alertInterval)
    state.tempNumberOfLogs = state.storeOfReceivedLogs.length
  }, alertInterval)

  fs.watchFile(filename, { interval: delay }, (curr, prev) => {
    const readstream = startStream(filename)

    readstream.on('line', (line) => {
      manageStream(line, state)
        .then(newState => newState)
        .catch(err => console.error(err))
    })
      .on('close', () => {
        manageCloseOfWatching(state.logs, state.alerts, state.storeOfReceivedLogs.length)
      })
  })
}

module.exports = {
  watch
}
