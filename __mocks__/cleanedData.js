const cleanedData = {
  dataOfCounts: [{ section: 'section5', count: 1, protocol: { http: 100, https: 0 } }],
  dataOfErrors: [{ section: 'section5', count: 1, percentage: 100 }],
  dataOfBytes: [{ section: 'section3', count: 135 }]
};

module.exports = cleanedData;