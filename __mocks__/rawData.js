const rawData = [{
  section: 'section5',
  countLogs: 1,
  errors: 1,
  errorPercentage: 100,
  requestCountByProtocol: { HTTP: 1, HTTPS: 0 },
  protocolPercentage: { http: 100, https: 0 },
  requestCountByMethods: { POST: 0, GET: 100, HEAD: 0, PUT: 0, PATCH: 0, DELETE: 0 },
  methodsPercentage: { POST: 0, GET: 100, HEAD: 0, PUT: 0, PATCH: 0, DELETE: 0 },
  bytes: 119,
  averageWeightOfRequest: 119
},
{
  section: 'section3',
  countLogs: 1,
  errors: 0,
  errorPercentage: 0,
  requestCountByProtocol: { HTTP: 0, HTTPS: 1 },
  protocolPercentage: { http: 0, https: 100 },
  requestCountByMethods: { POST: 100, GET: 0, HEAD: 0, PUT: 0, PATCH: 0, DELETE: 0 },
  methodsPercentage: { POST: 100, GET: 0, HEAD: 0, PUT: 0, PATCH: 0, DELETE: 0 },
  bytes: 135,
  averageWeightOfRequest: 135
}];

module.exports = rawData;