const logs = {
  section5:
    [{
      host: '63.60.59.143',
      logname: 'Hertha.Kub78',
      date: 1582617811000,
      method: 'GET',
      section: 'section5',
      protocol: 'HTTP',
      status: 544,
      bytes: 119
    }],
  section3:
    [{
      host: '53.254.241.217',
      logname: 'Dillan36',
      date: 1582617811000,
      method: 'POST',
      section: 'section3',
      protocol: 'HTTPS',
      status: 326,
      bytes: 135
    }]
}

module.exports = logs;