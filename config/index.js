const { watch } = require('../watcher')
const { displayResults } = require('../display')

/**
 * This is the starting function to display blank results and start watching file
 * @param {Object} args args from prompt
 * @returns {Void}
 */
const start = (args) => {
  displayResults()
  watch(args)
}

module.exports = {
  start
}
