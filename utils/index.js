/**
 * Calculate a percentage
 * @param {Number} part numerator
 * @param {Number} total denominator
 * @returns {Number}
 */
const getPercentage = (part, total) => Math.round((part / total) * 100 * 100) / 100

/**
 * Sort an array with any custom sorting function
 * @param {Array} array array to sort
 * @param {Function} query sorting function
 * @returns {Arrays<Object>}
 */
const sortArrayByQuery = (array, query) => {
  const sortedArray = array.sort(query)
  return sortedArray.length >= 3 ? [sortedArray[0], sortedArray[1], sortedArray[2]] : [sortedArray[0]]
}

/**
 * Convert seconds to milliseconds
 * @param {Number} number number in seconds
 * @returns {Number}
 */
const convertSecondsToMs = (number) => number * 1000

/**
 * Check if data that will be printed exists, otherwise it displays 0
 * @param {*} data any data to print in console
 * @returns {String|Number}
 */
const print = (data) => (data || 0)

module.exports = {
  getPercentage,
  sortArrayByQuery,
  convertSecondsToMs,
  print
}
