const { getPercentage, sortArrayByQuery, convertSecondsToMs, print } = require('./index');
const rawData = require('../__mocks__/rawData');
const sortedData = require('../__mocks__/sortedData');

describe('Utils module', () => {
  test('test function to get percentage', () => {
    expect(getPercentage(345, 890)).toBe(38.76);
  });

  test('test function to convert seconds in milliseconds', () => {
    expect(convertSecondsToMs(50)).toBe(50000);
  });

  test('test function that test data before printing', () => {
    expect(print(undefined)).toBe(0);
    expect(print('une histoire')).toBe('une histoire');
  });

  test('test function that sort an array by query and return top 3 or top 1', () => {
    expect(sortArrayByQuery(rawData, (a, b) => a.countLogs < b.countLogs)).toEqual(sortedData.sortedByCounts);
  })
})